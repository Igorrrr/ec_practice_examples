import numpy as np
from deap import base, creator
import random
from deap import tools
from tqdm import tqdm
import matplotlib.pyplot as plt
from matplotlib.pyplot import figure

problem = "xqf131"
x, y = [], []
f = open("{}".format(problem), "r")
for l in f:
    row = l.split()
    x.append(row[1])
    y.append(row[2])

x = [int(el) for el in x]
y = [int(el) for el in y]

cities = []
for i in range(len(x)):
    cities.append([x[i], y[i]])


def distance(c1, c2):
    xDis = abs(c1[0] - c2[0])
    yDis = abs(c1[1] - c2[1])
    dist = np.sqrt((xDis ** 2) + (yDis ** 2))
    return dist


distances = np.zeros((len(cities), len(cities)))
for i in range(len(cities)):
    for j in range(len(cities)):
        distances[i, j] = distance(cities[i], cities[j])

creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
creator.create("Individual", list, fitness=creator.FitnessMin)

toolbox = base.Toolbox()

toolbox.register("indices", random.sample, range(len(cities)), len(cities))

toolbox.register("individual", tools.initIterate, creator.Individual, toolbox.indices)

toolbox.register("population", tools.initRepeat, list, toolbox.individual)

print([individual[:10] for individual in toolbox.population(n=5)])


def EVALUATE(individual):
    summation = 0
    start = individual[0]
    for i in range(1, len(individual)):
        end = individual[i]
        summation += distances[start][end]
        start = end
    return summation


toolbox.register("evaluate", EVALUATE)


def MUTATE(individual, mutationRate):
    for swapped in range(len(individual)):
        if (random.random() < mutationRate):
            swapWith = int(random.random() * len(individual))
            city1 = individual[swapped]
            city2 = individual[swapWith]
            individual[swapped] = city2
            individual[swapWith] = city1
    return individual


toolbox.register("mate", tools.cxOrdered)
toolbox.register("mutate", MUTATE, mutationRate=0.01)
toolbox.register("select", tools.selTournament, tournsize=6)

POPULATION_SIZE = 200
N_ITERATIONS = 1000
N_MATINGS = 50

population = toolbox.population(n=POPULATION_SIZE)

fitnesses = [(individual, toolbox.evaluate(individual)) for individual in population]
for individual, fitness in fitnesses:
    individual.fitness.values = (fitness,)


def offsprings(population):
    for _ in range(N_MATINGS):
        i1, i2 = np.random.choice(range(len(population)), size=2, replace=False)
        offspring1, offspring2 = toolbox.mate(population[i1], population[i2])
        yield toolbox.mutate(offspring1)
        yield toolbox.mutate(offspring2)

plt.ion()

def plot_route(route_x, route_y):
    figure(2, figsize=(10, 10))
    plt.clf()
    plt.scatter(x, y)
    plt.plot(route_x, route_y)
    plt.xlim = (0, 100)
    plt.ylim = (0, 100)
    plt.pause(0.01)


from operator import itemgetter

fitness_plot = []
for iteration in tqdm(list(range(1, N_ITERATIONS + 1))):
    current_population = list(map(toolbox.clone, population))
    offspring = list(offsprings(current_population))
    for child in offspring:
        current_population.append(child)
    fitnesses = [(individual, toolbox.evaluate(individual)) for individual in current_population]
    for individual, fitness in fitnesses:
        individual.fitness.values = (fitness,)
    population[:] = toolbox.select(current_population, len(population))
    fitnesses = sorted([(i, toolbox.evaluate(individual)) for i, individual in enumerate(population)],
                       key=lambda x: x[1])
    fitness_plot.append(population[0].fitness.values[0])
    route_x = itemgetter(*population[0])(x)
    route_y = itemgetter(*population[0])(y)
    plot_route(route_x, route_y)

fitnesses = sorted([(i, toolbox.evaluate(individual)) for i, individual in enumerate(population)], key=lambda x: x[1])

print(fitnesses[:5])
plt.ioff()
route_x = itemgetter(*population[0])(x)
route_y = itemgetter(*population[0])(y)
plot_route(route_x, route_y)

plt.figure(3)
plt.plot(fitness_plot)
plt.show()


