from copy import deepcopy
from functools import partial
from statistics import mean
import uuid
from deap import tools
from deap.base import Toolbox
import numpy
from scheduling.src.algs.ga.GAFunctions2 import GAFunctions2
from scheduling.src.algs.ga.common_fixed_schedule_schema import run_ga, fit_converter
from scheduling.src.algs.heft.DSimpleHeft import run_heft
from scheduling.src.core.CommonComponents.ExperimentalManager import ExperimentResourceManager, ModelTimeEstimator
from scheduling.src.core.environment.ResourceManager import Schedule
from scheduling.src.core.environment.Utility import wf, Utility
from scheduling.src.core.environment.ResourceGenerator import ResourceGenerator as rg
from scheduling.src.algs.ga.common_fixed_schedule_schema import generate as ga_generate
from scheduling.src.algs.common.utilities import unzip_result
from scheduling.src.core.CommonComponents.utilities import repeat
from scheduling.src.experiments.common import AbstractExperiment


class GABaseExperiment(AbstractExperiment):

    @staticmethod
    def run(**kwargs):
        inst = GABaseExperiment(**kwargs)
        return inst()

    def __init__(self, ga_params=None):
        wf_name = "Montage_100"
        GA_PARAMS = {
            "kbest": 5,
            "n": 25,
            "cxpb": 0.3,  # 0.8
            "mutpb": 0.9,  # 0.5
            "sweepmutpb": 0.3,  # 0.4
            "gen_curr": 0,
            "gen_step": 300,
            "is_silent": False
        }
        if ga_params is None:
            self.GA_PARAMS = GA_PARAMS
        else:
            self.GA_PARAMS = ga_params
        self.wf_name = wf_name

    def __call__(self):
        _wf = wf(self.wf_name)
        rm = ExperimentResourceManager(rg.r([10, 15, 25, 30]))
        estimator = ModelTimeEstimator(bandwidth=10)

        empty_fixed_schedule_part = Schedule({node: [] for node in rm.get_nodes()})

        heft_schedule = run_heft(_wf, rm, estimator)

        fixed_schedule = empty_fixed_schedule_part

        ga_functions = GAFunctions2(_wf, rm, estimator)

        generate = partial(ga_generate, ga_functions=ga_functions,
                           fixed_schedule_part=fixed_schedule,
                           current_time=0.0, init_sched_percent=0.05,
                           initial_schedule=heft_schedule)

        stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        stats.register("avg", numpy.mean)
        stats.register("std", numpy.std)
        stats.register("min", numpy.min)
        stats.register("max", numpy.max)

        logbook = tools.Logbook()
        logbook.header = ["gen", "evals"] + stats.fields

        toolbox = Toolbox()
        toolbox.register("generate", generate)
        toolbox.register("evaluate", fit_converter(ga_functions.build_fitness(empty_fixed_schedule_part, 0.0)))
        toolbox.register("clone", deepcopy)
        toolbox.register("mate", ga_functions.crossover)
        toolbox.register("sweep_mutation", ga_functions.sweep_mutation)
        toolbox.register("mutate", ga_functions.mutation)
        # toolbox.register("select_parents", )
        # toolbox.register("select", tools.selTournament, tournsize=4)
        toolbox.register("select", tools.selRoulette)
        pop, logbook, best = run_ga(toolbox=toolbox,
                                logbook=logbook,
                                stats=stats,
                                **self.GA_PARAMS)

        resulted_schedule = ga_functions.build_schedule(best, empty_fixed_schedule_part, 0.0)

        ga_makespan = Utility.makespan(resulted_schedule)
        return (ga_makespan, resulted_schedule, logbook)


def fix_schedule(res, heft):
    for item in heft.mapping:
        res.mapping[item] = res.mapping[item].append(heft.mapping[item])
    return res

import matplotlib.pyplot as plt
import matplotlib.patches as patches
import seaborn as sns
def draw_schedule(schedule):
    n = len(schedule.get_all_unique_tasks())
    colors = sns.color_palette("Set2", n)
    fig, ax = plt.subplots(1)
    mapping = schedule.mapping
    m = len(mapping.keys())
    keys = list(mapping.keys())
    used_colors = 0
    for k in range(m):
        items = mapping[keys[k]]
        for it in items:
            print("Task {}, st {} end {}".format(it.job.id, it.start_time, it.end_time))
            coords = (it.start_time, k)
            rect = patches.Rectangle(coords, it.end_time - it.start_time, 1, fill=True, facecolor=colors[used_colors],
                                     label=it.job.id[4:7], alpha=0.5, edgecolor="black")
            used_colors += 1
            ax.add_patch(rect)
            ax.text(coords[0] + (it.end_time - it.start_time) / 3, coords[1] + 0.5, str(it.job.id[4:7]))

    # plt.legend()
    plt.ylim(0, m)
    makespan=max([mapping[v][-1].end_time for v in mapping.keys()])
    plt.xlim(0, makespan)
    # plt.show()
    # plt.savefig(os.path.join(path, "schedule_{}_{}.png".format(run_name, test_i)))
    plt.show()
    plt.close()

if __name__ == "__main__":
    exp = GABaseExperiment()
    repeat_count = 1
    # result, schedule, logbooks = unzip_result(repeat(exp, repeat_count))
    result, schedule, logbooks = exp()
    # logbook = logbooks_in_data(logbooks)
    draw_schedule(schedule[0])
    print(result)
    print(schedule)
