from deap import tools, base
from multiprocessing import Pool
from ga_scheme import eaMuPlusLambda
from numpy import random as rnd
import numpy as np
from deap import creator
from deap import benchmarks
from functions import rastrigin
from draw_log import draw_log
import random

creator.create("BaseFitness", base.Fitness, weights=(1.0,))
creator.create("Individual", np.ndarray, fitness=creator.BaseFitness)


def mutation(ind, p):
    ind_size = len(ind)
    for i in range(ind_size):
        if rnd.random() < p:
            i_swap_with = rnd.randint(0, ind_size-1)
            ind[i], ind[i_swap_with] = ind[i_swap_with], ind[i]
        else:
            pass
    return ind,


def crossover(ind1, ind2):
    for i, (val1, val2) in enumerate(zip(ind1, ind2)):
        prob = np.random.choice([0, 0.25, 0.5, 0.75, 1])
        ind1[i] = (1 - prob) * val1 + prob * val2
        ind2[i] = prob * val1 + (1 - prob) * val2
    return ind1, ind2


class SimpleGAExperiment:
    def factory(self):
        return rnd.random(self.dimension) * 10 - 5

    def __init__(self, function, dimension, pop_size, iterations):
        self.pop_size = pop_size
        self.iterations = iterations
        self.mut_prob = 0.5
        self.cross_prob = 0.7

        self.function = function
        self.dimension = dimension

        self.engine = base.Toolbox()
        self.engine.register("map", map)
        self.engine.register("individual", tools.initIterate, creator.Individual, self.factory)
        self.engine.register("population", tools.initRepeat, list, self.engine.individual, self.pop_size)
        self.engine.register("mate", crossover) 
        self.engine.register("mutate", mutation, p=0.5)
        self.engine.register("select", tools.selTournament, tournsize=4)
        self.engine.register("evaluate", self.function)

    def run(self):
        pop = self.engine.population()
        hof = tools.HallOfFame(3, np.array_equal)
        stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)

        pop, log = eaMuPlusLambda(pop, self.engine, mu=self.pop_size, lambda_=int(self.pop_size*0.9), cxpb=self.cross_prob, mutpb=self.mut_prob,
                                  ngen=self.iterations,
                                  stats=stats, halloffame=hof, verbose=True)
        print(f"Best = {hof[0]}")
        print(f"Best fit = {hof[0].fitness.values[0]}")
        return log

if __name__ == "__main__":

    def function(x):
        res = rastrigin(x)
        return res,

    dimension = 100
    pop_size = 100
    iterations = 100
    scenario = SimpleGAExperiment(function, dimension, pop_size, iterations)
    log = scenario.run()
    draw_log(log)
