from deap import tools, base, creator
import numpy as np
from ga_scheme import eaMuPlusLambda
import numpy.random as rnd
from tensorflow.keras import Sequential
from tensorflow.keras.layers import Dense, InputLayer
import gym
from draw_log import draw_log
from copy import deepcopy

creator.create("BaseFitness", base.Fitness, weights=(1.0, ))
creator.create("Individual", list, fitness=creator.BaseFitness)

np.random.seed(42)

class RL_ga_experiment:

    def factory(self):
        ind = []
        for i in range(len(self.params)):
            if i % 2 == 0:
                ind.append(rnd.normal(0.1, 0.3, size=self.params[i].shape))
            else:
                ind.append(np.zeros(shape=self.params[i].shape))
        return creator.Individual(ind)

    def mutation(self, ind, p):
        ind_size = len(ind)
        for i in range(ind_size):
            if i % 2 == 0:
                for j in range(len(ind[i])):
                    for k in range(len(ind[i][j])):
                        cur_prob = np.random.random()
                        if cur_prob < p:
                            i_swap_with = rnd.randint(0, len(ind[i][j])-1)
                            ind[i][j][k], ind[i][j][i_swap_with] = ind[i][j][i_swap_with], ind[i][j][k]
                            ind[i][j] += rnd.normal(0.0, 0.5)
                        else:
                            ind[i][j] -= rnd.normal(0.0, 0.3)
                    if cur_prob < p:
                        i_swap_with = rnd.randint(0, len(ind[i])-1)
                        ind[i][j], ind[i][i_swap_with] = ind[i][i_swap_with], ind[i][j]
        return ind,


    def crossover(self, p1, p2):
        c1, c2 = [], []
        for i in range(len(p1)):
            p_size = len(p1[i]) // 2
            if not i % 2: 
                p = np.random.random()
                lhs_p1 = p1[i][:p_size]
                rhs_p1 = p1[i][p_size:]
                lhs_p2 = p2[i][:p_size]
                rhs_p2 = p2[i][p_size:]
                if p < 0.5:
                    left = np.append(lhs_p1, rhs_p2, 0)
                    right = np.append(lhs_p2, rhs_p1, 0)
                else:
                    left = np.append(lhs_p2, rhs_p1, 0)
                    right = np.append(lhs_p1, rhs_p2, 0)
                c1.append(left)
                c2.append(right)
            else:
                c1.append(p1[i])
                c2.append(p2[i])
        return creator.Individual(c1), creator.Individual(c2)

    def __init__(self, input_dim, l1, l2, output_dim, pop_size, iterations):
        self.input_dim = input_dim
        self.output_dim = output_dim
        self.l1 = l1
        self.l2 = l2
        self.pop_size = pop_size
        self.iterations = iterations
        self.cross_prob = 0.4
        self.mut_prob = 0.8

        self.model = self.build_model()
        self.params = self.model.get_weights()
        self.env = gym.make("LunarLander-v2")

        self.engine = base.Toolbox()
        self.engine.register('map', map)
        self.engine.register("individual", tools.initIterate, creator.Individual, self.factory)
        self.engine.register('population', tools.initRepeat, list, self.engine.individual, self.pop_size)
        self.engine.register('mutate', self.mutation, p=0.6)
        self.engine.register("mate", self.crossover)
        self.engine.register('select', tools.selTournament, tournsize=3)
        self.engine.register('evaluate', self.fitness)

    def compare(self, ind1, ind2):
        result = True
        for i in range(len(ind1)):
            if i % 2 == 0:
                for j in range(len(ind1[i])):
                    for k in range(len(ind1[i][j])):
                        if ind1[i][j][k] != ind2[i][j][k]:
                            return False
        return result

    def run(self):
        pop = self.engine.population()
        hof = tools.HallOfFame(3, similar=self.compare)
        stats = tools.Statistics(lambda ind: ind.fitness.values[0])
        stats.register('min', np.min)
        stats.register('max', np.max)
        stats.register('avg', np.mean)
        stats.register('std', np.std)

        pop, log = eaMuPlusLambda(pop, self.engine, 
                                mu=self.pop_size,
                                lambda_=int(0.8 * self.pop_size),
                                cxpb=self.cross_prob,
                                mutpb=self.mut_prob,
                                ngen=self.iterations,
                                halloffame=hof,
                                stats=stats,
                                verbose=True)
        best = hof[0]
        print("Best fitness = {}".format(best.fitness.values[0]))
        return log, best


    def build_model(self):
        model = Sequential()
        model.add(InputLayer(self.input_dim))
        model.add(Dense(self.l1, activation='relu'))
        model.add(Dense(self.l2, activation='relu'))
        model.add(Dense(self.output_dim, activation='softmax'))
        model.compile(optimizer='adam', loss='mse')
        return model

    def fitness(self, ind):
        self.model.set_weights(ind)
        scores = []
        state = self.env.reset()
        score = 0.0
        for t in range(self.iterations):
            self.env.render()
            act_prob = self.model.predict(state.reshape(1, self.input_dim)).squeeze()
            action = rnd.choice(np.arange(self.output_dim), 1, p=act_prob)[0]
            next_state, reward, done, _ = self.env.step(action)
            score += reward
            state = next_state
            if done:
                break
        scores.append(score)
        return np.mean(scores),

if __name__ == '__main__':
    input_dim = 8
    l1 = 200
    l2 = 120
    output_dim = 4
    pop_size = 50
    iterations = 100

    exp = RL_ga_experiment(input_dim, l1, l2, output_dim, pop_size, iterations)
    log, best = exp.run()

    draw_log(log)

    for _ in range(100):
        exp.fitness(best)
